﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {
    public GameObject projectile;

    public float projectileSpeed = 10;
    public float health = 150;
    public float shotsPerSeconds = 0.5f;

    void OnTriggerEnter2D(Collider2D collider)
    {
        //grab gameObject collider, get projectile component 
        ProjectileScript missile = collider.gameObject.GetComponent<ProjectileScript>();
        if (missile) {
            health -= missile.GetDamage();
            //tell missile its collided
            missile.Hit();
            if (health <=0)
            {
                Destroy(gameObject);
            }
            Debug.Log("Hit by a projectile");
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //shoot every two seconds
        float probability = Time.deltaTime * shotsPerSeconds;
        if (Random.value < probability)
        {
            Fire();
        }
        

	}

    void Fire()
    {
        //creating projectiles
        Vector3 startPosition = transform.position + new Vector3(0, -1, 0);
        GameObject missile = Instantiate(projectile, startPosition, Quaternion.identity) as GameObject;
        //giving projectiles velocity so they fall downwards
        missile.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -projectileSpeed);
    }
}
